import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:router_test/provider/app_state_provider.dart';
import 'package:router_test/router/app_pages.dart';
import 'package:router_test/utils/router.dart';
import 'package:router_test/widget/modal/full_screen_modal_container.dart';

class AuthModal extends StatelessWidget {
  const AuthModal({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FullScreenModalContainer(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Authenticate"),
          ElevatedButton(
              onPressed: () {
                Provider.of<AppStateProvider>(context, listen: false).login();
                routerNavigate(context, PageSecondPath);
              },
              child: Text("Authenticate"))
        ],
      ),
    );
  }
}
