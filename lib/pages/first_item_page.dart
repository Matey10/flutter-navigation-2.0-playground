import 'package:flutter/material.dart';
import 'package:router_test/router/app_pages.dart';
import 'package:router_test/utils/router.dart';
import 'package:router_test/widget/centered_title.dart';

class FirstItemPage extends StatelessWidget {
  const FirstItemPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final num id = num.parse(routerGetParams(context)["id"]!);

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Item id: ${id}"),
        ElevatedButton(onPressed: () {
          routerNavigate(context, PageFirstPath);
        }, child: Text("Back")),
      ],
    );
  }
}
