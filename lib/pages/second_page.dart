import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:router_test/provider/app_state_provider.dart';
import 'package:router_test/router/app_pages.dart';
import 'package:router_test/utils/router.dart';

class SecondPage extends StatelessWidget {
  const SecondPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    printLocation(context);

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Second Page"),
          Consumer<AppStateProvider>(
            builder: (context, provider, child) {
              final isLoggedIn = provider.isLoggedIn;

              return ElevatedButton(
                onPressed: () {
                  if (isLoggedIn) {
                    routerNavigate(context, PagePathSecret);
                  } else {
                    routerQueryNavigate(context, QueryPageAuthPath);
                  }
                },
                child: Text(isLoggedIn ? "Show secret page" : "Authenticate to see secret page"),
              );
            },
          ),
        ],
      ),
    );
  }
}
