import 'package:flutter/material.dart';
import 'package:router_test/router/app_pages.dart';
import 'package:router_test/utils/router.dart';
import 'package:router_test/widget/timer_pro.dart';

class FirstPage extends StatelessWidget {
  const FirstPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    printLocation(context);

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(mainAxisAlignment: MainAxisAlignment.center,
          children: [
          Text("First Page with timer "),
          TimerPro(),
        ],),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
                onPressed: () {
                  routerNavigate(context, PagePathFirstItem, { "id": "1" });
                },
                child: Text('/1'),
            ),
            SizedBox(width: 5),
            ElevatedButton(
              onPressed: () {
                routerNavigate(context, PagePathFirstItem, { "id": "2" });
              },
              child: Text('/2'),
            ),
            SizedBox(width: 5),
            ElevatedButton(
              onPressed: () {
                routerNavigate(context, PagePathFirstItem, { "id": "3" });
              },
              child: Text('/something_special'),
            ),
          ],
        )
      ],
    );
  }
}
