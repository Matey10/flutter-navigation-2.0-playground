import 'package:flutter/material.dart';
import 'package:router_test/widget/centered_title.dart';

class NotFoundPage extends StatelessWidget {
  const NotFoundPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CenteredTitle("Page Not Found");
  }
}
