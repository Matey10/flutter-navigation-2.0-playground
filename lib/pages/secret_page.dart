import 'package:flutter/material.dart';
import 'package:router_test/widget/centered_title.dart';

class SecretPage extends StatelessWidget {
  const SecretPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.pinkAccent,
        child: CenteredTitle(
            "You are awesome, but it is between you and me"
        )
    );
  }
}
