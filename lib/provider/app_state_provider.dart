import 'package:flutter/material.dart';

class AppStateProvider extends ChangeNotifier {
  bool isAppResolved = false;
  bool isLoggedIn = false;

  void resolveApp () {
    isAppResolved = true;
    notifyListeners();
  }

  void login () {
    isLoggedIn = true;
    notifyListeners();
  }

  void logout () {
    isLoggedIn = false;
    notifyListeners();
  }
}
