import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:router_test/provider/app_state_provider.dart';
import 'package:router_test/router/app_router_delegate.dart';
import 'package:router_test/router/app_route_information_parser.dart';

class AppRouter extends StatelessWidget {
  const AppRouter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appState = Provider.of<AppStateProvider>(context, listen: false);

    return Router(
      routerDelegate: AppRouterDelegate(appState: appState),
      routeInformationParser: AppRouteInformationParser(),
    );
  }
}
