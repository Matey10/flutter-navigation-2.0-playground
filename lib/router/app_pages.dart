import 'package:fluri/fluri.dart';
import 'package:flutter/material.dart';
import 'package:router_test/modals/auth_modal.dart';
import 'package:router_test/pages/first_page.dart';
import 'package:router_test/pages/first_item_page.dart';
import 'package:router_test/pages/not_found_page.dart';
import 'package:router_test/pages/second_page.dart';
import 'package:router_test/pages/secret_page.dart';

// PagePaths
const PagePathFirstItem = "/first/:id";
const PageFirstPath = "/first";
const PageSecondPath = "/second";
const PagePathSecret = "/secret";

const PagePathNotFound = "/not_found";

// QueryPagePaths
const QueryPageAuthPath = "auth";

// Available paths
// They should be ordered in next way:
// /asd/bsd/csd
// /asd/bsd
// /asd
// To prevent /asd match instead of /asd/bsd/csd
const List<String> AvailablePathList = [
  PagePathFirstItem,
  PageFirstPath,
  PageSecondPath,
  PagePathSecret,
];

final AvailablePathSegmentsList = AvailablePathList
    .map((path) => Fluri(path).pathSegments.toList(growable: false))
    .toList(growable: false);

// Protected paths
List<String> ProtectedPathList = [
  PagePathSecret,
];

class PagePathInfo {
  final String path;
  final List<String> pathSegments;
  final List<String> locationSegments;

  PagePathInfo({
    required this.path,
    required this.pathSegments,
    required this.locationSegments,
  });
}

PagePathInfo getPagePathFromLocation(String location) {
  final locationSegments = Fluri(location).pathSegments.toList();

  final pathIndex = AvailablePathSegmentsList.indexWhere((segments) {
    if (segments.length != locationSegments.length) {
      return false;
    }

    for (var i = 0; i < segments.length; i++) {
      final currentSegment = segments[i];

      if (currentSegment.startsWith(":")) continue;
      if (currentSegment != locationSegments[i]) return false;
    }

    return true;
  });

  if (pathIndex == -1) {
    return PagePathInfo(
        locationSegments: locationSegments,
        path: PagePathNotFound,
        pathSegments: []
    );
  }

  return PagePathInfo(
      locationSegments: locationSegments,
      path: AvailablePathList[pathIndex],
      pathSegments: AvailablePathSegmentsList[pathIndex],
  );
}

Widget getPageWidget(String path) {
  switch(path) {
    case PagePathFirstItem: return FirstItemPage();
    case PageFirstPath: return FirstPage();
    case PageSecondPath: return SecondPage();
    case PagePathSecret: return SecretPage();
  }

  return NotFoundPage();
}

Widget? getQueryPageWidget(String path) {
  switch(path) {
    case QueryPageAuthPath: return AuthModal();
  }

  return null;
}

bool getIsPageProtected(String path) {
  return ProtectedPathList.contains(path);
}

