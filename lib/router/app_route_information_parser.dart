import 'package:flutter/material.dart';
import 'package:router_test/router/app_page_configuration.dart';

class AppRouteInformationParser
  extends RouteInformationParser<PageConfiguration> {
  @override
  Future<PageConfiguration> parseRouteInformation(
    RouteInformation routeInformation
  ) {
    return Future.value(PageConfiguration(location: routeInformation.location!));
  }
}