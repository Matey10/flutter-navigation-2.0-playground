import 'package:flutter/material.dart';
import 'package:router_test/provider/app_state_provider.dart';
import 'package:router_test/router/app_page_configuration.dart';
import 'package:router_test/router/app_pages.dart';
import 'package:router_test/widget/bottom_navigation/bottom_navigation.dart';
import 'package:router_test/widget/header.dart';

final GlobalKey<NavigatorState> navKey = GlobalKey();

class AppRouterDelegate extends RouterDelegate<PageConfiguration>
  with ChangeNotifier, PopNavigatorRouterDelegateMixin {

  final AppStateProvider appState;
  late PageConfiguration _currentConfiguration;

  AppRouterDelegate({ required this.appState }) {
    _currentConfiguration = PageConfiguration(location: "/first");

    // Navigate user from protected page on logout
    appState.addListener(() {
      if (ProtectedPathList.contains(_currentConfiguration.path)) {
        setNewRoutePath(PageConfiguration(location: PageFirstPath));
      }
    });
  }
  
  @override
  final GlobalKey<NavigatorState> navigatorKey = navKey;

  @override
  Future<void> setNewRoutePath(PageConfiguration configuration) async {
    _currentConfiguration = configuration;
    notifyListeners();
  }

  bool onPopPage (Route<dynamic> route, dynamic params) {
    return false;
  }

  List<MaterialPage> get _buildPages {
    return currentConfiguration.pathList.map((e) {
      return MaterialPage(
        key: ValueKey(e),
        child: getPageWidget(e),
      );
    }).toList(growable: false);
  }

  Widget? get _buildQueryNavigation {
    final queryPath = currentConfiguration.getQueryParameter("cmd");

    if (queryPath == null) {
      return null;
    }

    final page = getQueryPageWidget(queryPath);

    if (page == null) {
      return null;
    }

    return Positioned(top: 0, bottom: 0, left: 0, right: 0, child: Navigator(
      onPopPage: onPopPage,
      pages: [
        MaterialPage(key: ValueKey(queryPath), child: page)
      ],
    ));
  }

  @override
  PageConfiguration get currentConfiguration => _currentConfiguration;

  @override
  Widget build(BuildContext context) {
    print("Delegate build with next configuration\n"
        "Location: ${currentConfiguration.location}\n"
        "Path: ${currentConfiguration.path}\n"
        "Paths: ${currentConfiguration.pathList}\n"
        "QueryLocation: ${currentConfiguration.getQueryParameter("cmd")}");

    final queryNavigation = _buildQueryNavigation;

    return Stack(
      children: [
        Column(
          children: [
            Header(),
            Expanded(
              child: Navigator(
                key: navigatorKey,
                onPopPage: onPopPage,
                pages: _buildPages,
              ),
            ),
            BottomNavigation(),
          ],
        ),
        if (queryNavigation != null) queryNavigation,
      ],
    );
  }
}