import 'package:fluri/fluri.dart';
import 'package:router_test/constants/router.dart';
import 'package:router_test/router/app_pages.dart';

class PageConfiguration {
  final String location;
  late List<String> locationSegments;
  late String path;
  late List<String> pathSegments;
  late List<String> pathList;

  PageConfiguration({ required this.location }) {
    final pathInfo = getPagePathFromLocation(location);

    path = pathInfo.path;
    pathSegments = pathInfo.pathSegments;
    locationSegments = pathInfo.locationSegments;

    final List<String> collectedPaths = [];
    String collectedLocation = '';

    for (var segment in pathSegments) {
      final segmentPath = '/$segment';
      final fullPath = '$collectedLocation$segmentPath';

      collectedPaths.add(fullPath);
      collectedLocation += segmentPath;
    }

    pathList = collectedPaths;
  }

  String get pathname {
    return Fluri(location).path;
  }

  String get query {
    return Fluri(location).query;
  }

  Map<String, String> get queryParameters {
    return Fluri(location).queryParameters;
  }


  String? getQueryParameter(String parameterName) {
    return Fluri(location).queryParameters[parameterName];
  }

  String? get queryRoute {
    return getQueryParameter(QueryPageKey);
  }

  bool get isProtected {
    return getIsPageProtected(pathname);
  }
}