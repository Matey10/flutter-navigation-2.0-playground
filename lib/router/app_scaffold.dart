import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:router_test/provider/app_state_provider.dart';
import 'package:router_test/router/app_router.dart';
import 'package:router_test/router/app_splash_screen.dart';

class AppScaffold extends StatelessWidget {
  const AppScaffold({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final isAppResolved = Provider.of<AppStateProvider>(context).isAppResolved;

    return Scaffold(
      body: isAppResolved ? const AppRouter() : const AppSplashScreen(),
    );
  }
}
