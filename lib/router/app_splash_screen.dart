import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:router_test/provider/app_state_provider.dart';
import 'package:router_test/widget/centered_title.dart';

class AppSplashScreen extends StatefulWidget {
  const AppSplashScreen({Key? key}) : super(key: key);

  @override
  State<AppSplashScreen> createState() => _AppSplashScreenState();
}

class _AppSplashScreenState extends State<AppSplashScreen> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((timeStamp) async {
      Future.delayed(const Duration(milliseconds: 2000), () {
        Provider.of<AppStateProvider>(context, listen: false).resolveApp();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.teal, child: const CenteredTitle("Splash Screen", color: Colors.white));
  }
}
