import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:router_test/provider/app_state_provider.dart';
import 'package:router_test/router/app_pages.dart';
import 'package:router_test/utils/router.dart';

class Header extends StatelessWidget {
  const Header({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final topOffset = MediaQuery.of(context).padding.top;

    return Container(
      color: Colors.blue,
      padding: EdgeInsets.only(top: topOffset),
      height: topOffset + 50,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("Header", style: TextStyle(fontSize: 16),),
            Consumer<AppStateProvider>(
              builder: (context, provider, child) {
                final isLoggedIn = provider.isLoggedIn;

                return ElevatedButton(
                    onPressed: () {
                      if (isLoggedIn) {
                        provider.logout();
                      } else {
                        routerQueryNavigate(context, QueryPageAuthPath);
                      }
                    },
                    child: Text(isLoggedIn ? "Logout" : "Auth"),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
