import 'package:flutter/material.dart';

class FullScreenModalContainer extends StatelessWidget {
  final Widget? child;

  const FullScreenModalContainer({this.child, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final mediaQueryPadding = MediaQuery.of(context).padding;

    return Container(
      color: Colors.cyan,
      padding: EdgeInsets.only(
          top: mediaQueryPadding.top,
          bottom: mediaQueryPadding.bottom
      ),
      child: child,
    );
  }
}
