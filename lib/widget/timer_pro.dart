import 'dart:async';

import 'package:flutter/material.dart';

class TimerPro extends StatefulWidget {
  const TimerPro({Key? key}) : super(key: key);

  @override
  _TimerProState createState() => _TimerProState();
}

class _TimerProState extends State<TimerPro> {
  int counter = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer.periodic(Duration(seconds: 2), (timer) {
      setState(() {
        counter += 1;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Text(counter.toString());
  }
}
