import 'package:flutter/material.dart';
import 'package:router_test/router/app_pages.dart';
import 'package:router_test/widget/bottom_navigation/bottom_navigation_item.dart';

class BottomNavigation extends StatelessWidget {
  const BottomNavigation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      color: Colors.lightBlueAccent,
      child: Row(
        children: [
          Expanded(child: BottomNavigationItem(text: 'First', path: PageFirstPath)),
          Expanded(child: BottomNavigationItem(text: 'Second', path: PageSecondPath)),
        ],
      ),
    );
  }
}
