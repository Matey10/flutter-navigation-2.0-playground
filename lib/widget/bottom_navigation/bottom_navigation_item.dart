import 'package:flutter/material.dart';
import 'package:router_test/utils/router.dart';

class BottomNavigationItem extends StatelessWidget {
  final String text;
  final String path;

  const BottomNavigationItem({required this.text, required this.path, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final hasMatch = getPageConfiguration(context).pathList.contains(path);

    return GestureDetector(
      onTap: () {
        routerNavigate(context, path);
      },
      child: Container(
        color: hasMatch ? Colors.white10 : Colors.transparent,
        child: Center(
          child: Text(
            text,
            style: TextStyle(
                decoration:
                    hasMatch ? TextDecoration.underline : TextDecoration.none),
          ),
        ),
      ),
    );
  }
}
