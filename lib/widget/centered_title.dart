import 'package:flutter/material.dart';

class CenteredTitle extends StatelessWidget {
  final String text;
  final Color color;

  const CenteredTitle(this.text, {Key? key, this.color = Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child:
          Text(text, style: TextStyle(fontSize: 16, color: color, height: 1.2)),
    );
  }
}
