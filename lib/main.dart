import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:router_test/provider/app_state_provider.dart';
import 'package:router_test/router/app_scaffold.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    print("Building MyApp ${DateTime.now().millisecondsSinceEpoch}");

    return MultiProvider(
      providers: [ChangeNotifierProvider(create: (ctx) => AppStateProvider())],
      child: MaterialApp(
        title: 'Router Test',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const AppScaffold(),
      ),
    );
  }
}
