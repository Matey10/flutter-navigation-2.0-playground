import 'package:fluri/fluri.dart';
import 'package:flutter/material.dart';
import 'package:router_test/constants/router.dart';
import 'package:router_test/router/app_page_configuration.dart';
import 'package:router_test/router/app_pages.dart';

PageConfiguration getPageConfiguration(BuildContext context) {
  return Router.of(context).routerDelegate.currentConfiguration as PageConfiguration;
}

Map<String, String> routerGetParams(BuildContext context) {
  final Map<String, String> params = {};
  final configuration = getPageConfiguration(context);
  final pathSegments = configuration.pathSegments;
  final locationSegments = configuration.locationSegments;

  for (int i = 0; i < pathSegments.length; i++) {
    final pathSegmentItem = pathSegments[i];

    if (pathSegmentItem.startsWith(":")) {
      final parameter = pathSegmentItem.substring(1);

      params[parameter] = locationSegments[i];
    }
  }

  return params;
}

String generateLocationFromPath(String path, Map<String, String> params) {
  if (params.isEmpty) {
    return path;
  }

  String generatedLocation = path;

  for (final param in params.keys) {
    generatedLocation = generatedLocation.replaceAll(":$param", params[param]!);
  }
  
  return generatedLocation;
}

void routerNavigate(BuildContext context, String path, [Map<String, String> params = const {}]) {
  Router.of(context).routerDelegate.setNewRoutePath(
      PageConfiguration(location: generateLocationFromPath(path, params))
  );
}

void routerQueryNavigate(BuildContext context, String path) {
  final delegate = Router.of(context).routerDelegate;
  final currentConfiguration = delegate
      .currentConfiguration as PageConfiguration;

  final newPath = Fluri(currentConfiguration.location);
  newPath.setQueryParam(QueryPageKey, QueryPageAuthPath);

  Router.of(context).routerDelegate.setNewRoutePath(
      PageConfiguration(location: newPath.toString())
  );
}

void routerQueryClose(BuildContext context) {
  final delegate = Router.of(context).routerDelegate;
  final currentConfiguration = delegate
      .currentConfiguration as PageConfiguration;

  final newPath = Fluri(currentConfiguration.location);
  newPath.removeQueryParam(QueryPageKey);

  Router.of(context).routerDelegate.setNewRoutePath(
      PageConfiguration(location: newPath.toString())
  );
}

void printLocation(BuildContext context) {
  print("${getPageConfiguration(context).location} active");
}